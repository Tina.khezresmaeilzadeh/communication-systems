# Communication Systems 


**HW1:** Simulating Communication Channel with Distortion and Designing Hilbert Filter

**HW2:** Simulation of Various Types of AM Modulation and Demodulation Methods

**HW3:** Simulating FM Modulation and Demodulation

**Final Project:** Implementation of Shannon-Fano Algorithm and Simulation of Digital Modulation and Demodulation


