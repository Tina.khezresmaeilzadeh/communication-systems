% Phase one
%% Source Encoder and Source decoder
clear; clc;
pic1 = imread('1.gif');
pic = pic1;
pic_size = 64;
pic = imresize(pic,pic_size/512);
repeat = zeros(1,256); 
for i = 1:pic_size
    for j=1:pic_size 
        repeat(pic(i,j)+1) = repeat(pic(i,j)+1) + 1;
    end
end
probability = zeros(2,length(repeat));
probability(1,:) = 0:255;
probability(2,:) = repeat / (pic_size*pic_size);

% encoder
C = 2*ones(256,11);
table = main_recursive_func(probability,C);
encoded = [];
for i = 1 : pic_size
    for j = 1 : pic_size
        index = find(table(pic(i,j)+1,:) == 2);
        encoded = [encoded table(pic(i,j)+1,1:index(1)-1)];
    end
end

% decoder
symbols = [];
string = encoded;
while 1
   for i = 1:256
       index = find(table(i,:) == 2);
       if index(1)-1 <= length(string)
           if sum(string(1:index(1)-1) == table(i,1:index(1)-1)) == length(string(1:index(1)-1))
              symbols = [symbols i-1];
              string(1:index(1)-1) = [];
              break
           end
       end
   end
   if length(string) < 1
       break
   end
end
output_pic = zeros(64,64);
for i = 1 : length(symbols)
   row = ceil(i/64);
   column = i - (row-1)*64;
   output_pic(row,column) = symbols(i);
end

imshow(uint8(output_pic))

%% phase2
% Alef
clc;
fs = 100e3;
Ts = 30e-3;
fc = 10e3;

modulator_input = zeros(1,floor(length(encoded)));
for i = 1:floor(length(encoded))
    if encoded(i) == 0
       modulator_input(i) = 1; 
    else
       modulator_input(i) = 2; 
    end
end
%%
t1 = 0:(1/fs):(Ts/2);
value1 = sqrt(4/Ts)*cos(2*pi*fc*t1);
zero_numbers = Ts*fs - length(value1);
zero_part = zeros(1,zero_numbers);
basis1 = [value1 zero_part];
t2 = 0:(1/fs):Ts-1/fs;
basis2 = sqrt(2/Ts)*cos(2*pi*fc*t2);

a1 = corr2(basis1,basis1);
a2 = corr2(basis1,basis2);
a3 = corr2(basis2,basis2);
detection_table = [a1 a2;a2 a3];

%%
% Moddulator
modulated_signal = modulator(modulator_input,2,30e-3,10e3,100e3);
%%
% Channel
load BPF_Channel.mat
received_signal = filter(BPF_Channel,1,modulated_signal);

% Demodulator
corr_vector = demodulator(modulated_signal,Ts,fs,basis1,basis2);
%%
% Detector
symbols_reconstructed = detector(corr_vector,detection_table);
%%
bit_string = zeros(1,length(symbols_reconstructed));
for i = 1 : length(symbols_reconstructed)
    if symbols_reconstructed(i) == 1
        bit_string(i) = 0;
    else
        bit_string(i) = 1;
    end
end
%%
% Source decoder
pic_reconstructed = source_decoder(bit_string,table);
%%
imshow(uint8(pic_reconstructed))

%% Pe
clc;
spectrum_modulated_signal = f_trans(modulated_signal,length(modulated_signal));
%%
plot(abs(spectrum_modulated_signal))
%%
Index = find( abs(spectrum_modulated_signal) == max(abs(spectrum_modulated_signal)) );
total_energy = sum(abs(spectrum_modulated_signal).^2)/2;
starting = 0.5 * length(spectrum_modulated_signal);


for n = 1000 : starting
   Part_Of_Signal = spectrum_modulated_signal(Index(2) - n : Index(2) + n);
   if sum((abs(Part_Of_Signal)).^2) >= 0.99*0.5*total_energy
      break
   end
end
ninety_percent_of_energy = zeros(size(spectrum_modulated_signal));
ninety_percent_of_energy(Index(2) - n : Index(2) + n) =  spectrum_modulated_signal(Index(2) - n : Index(2) + n);
ninety_percent_of_energy(Index(1) - n : Index(1) + n) =  spectrum_modulated_signal(Index(1) - n : Index(1) + n);
%%
plot(abs(ninety_percent_of_energy));
grid on
title('Signal passed from channel')
xlabel('f')
ylabel('Signal passed from channel')
%% Mohasebeye Pahnaye band


BW = ((Index(1) - n)*100e3)*4;

%% Te Jim
 clc;
 noise1 = wgn(1,length(modulated_signal),5);
 noisy_signal1 = modulated_signal + noise1;

%%
noise2 = wgn(1,length(modulated_signal),6);
noisy_signal2 = modulated_signal + noise2;
%%
noise3 = wgn(1,length(modulated_signal),7);
noisy_signal3 = modulated_signal + noise3;
%%
noise4 = wgn(1,length(modulated_signal),8);
noisy_signal4 = modulated_signal + noise4;
%%
noise5 = wgn(1,length(modulated_signal),9);
noisy_signal5 = modulated_signal + noise5;
%%
noise6 = wgn(1,length(modulated_signal),12);
noisy_signal6 = modulated_signal + noise6;
 %%
 load BPF_Channel.mat
 [out_pic1,corr_vector1] =  reconstruction(noisy_signal1,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num1 = length(find(out_pic1 ~= pic));
 figure
 imshow(uint8(out_pic1))
 figure
 scatter(corr_vector1(1,:), corr_vector1(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)
 %%
 load BPF_Channel.mat
 [out_pic2,corr_vector2] =  reconstruction(noisy_signal2,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num2 = length(find(out_pic2 ~= pic));
 figure
 imshow(uint8(out_pic2))
 figure
 scatter(corr_vector2(1,:), corr_vector2(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)
 %%
 load BPF_Channel.mat
 [out_pic3,corr_vector3] =  reconstruction(noisy_signal3,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num3 = length(find(out_pic3 ~= pic));
 figure
 imshow(uint8(out_pic3))
 figure
 scatter(corr_vector3(1,:), corr_vector3(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)

 %%
 load BPF_Channel.mat
 [out_pic4,corr_vector4] =  reconstruction(noisy_signal4,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num4 = length(find(out_pic4 ~= pic));
 figure
 imshow(uint8(out_pic4))
 figure
 scatter(corr_vector4(1,:), corr_vector4(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)
 %%
 load BPF_Channel.mat
 [out_pic5,corr_vector5] =  reconstruction(noisy_signal5,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num5 = length(find(out_pic5 ~= pic));
 figure
 imshow(uint8(out_pic5))
 figure
 scatter(corr_vector5(1,:), corr_vector5(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)
 %%
 load BPF_Channel.mat
 [out_pic6,corr_vector6] =  reconstruction(noisy_signal6,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table);
 error_num6 = length(find(out_pic6 ~= pic));
 figure
 imshow(uint8(out_pic6))
 figure
 scatter(corr_vector6(1,:), corr_vector6(2,:));
 hold on
 t = 0:100;
 y = t+0.7076;
 plot(t,y)
%%
error_probability = [error_num1 error_num2 error_num3 error_num4 error_num5 error_num6 ]/(64*64);
stem(error_probability)
grid on
 
%% Functions

function C_Final = main_recursive_func(Input,C)
[left_part right_part] = split(Input);
C_temp = place_code(C,left_part,0);
C_temp = place_code(C_temp,right_part,1);
if size(left_part,2) > 1
    C_temp = main_recursive_func(left_part,C_temp);
end
if size(right_part,2) > 1
    C_temp = main_recursive_func(right_part,C_temp);
end

C_Final = C_temp;
end





function [b c] = split(a)
b = [];
c = [];
a_sorted = zeros(2,size(a,2));
[sorted_value index] = sort(a(2,:),'descend');
a_sorted(1,:) = a(1,index);
a_sorted(2,:) = sorted_value;

i = 1;
while i <= size(a_sorted,2)
    if i == 1
        b = [a_sorted(1,i);a_sorted(2,i)];
    elseif i == 2;
        c = [a_sorted(1,i);a_sorted(2,i)];
    elseif a_sorted(2,i) == 0
        if size(b,2) < size(c,2)
            b = [b [a_sorted(1,i);a_sorted(2,i)]];
        else
            c = [c [a_sorted(1,i);a_sorted(2,i)]];
        end
    elseif sum(b(2,:)) < sum(c(2,:))
        b = [b [a_sorted(1,i);a_sorted(2,i)]];
    else
        c = [c [a_sorted(1,i);a_sorted(2,i)]];
    end 
    i = i+1; 
end

end

% function [b c] = split(a)
% b = [];
% c = [];
% a_sorted = zeros(2,size(a,2));
% [sorted_value index] = sort(a(2,:),'descend');
% a_sorted(1,:) = a(1,index);
% a_sorted(2,:) = sorted_value;
% 
% for k = 1 : size(a_sorted,2)
%    if sum(a_sorted(2,1:k)) > sum(a_sorted(2,k+1:end))
%       border = k-1;
%       break
%    end
% end
% b = a_sorted(:,1:border);
% c = a_sorted(:,border+1:end);
% end

function Output = place_code(C,part,zerone)
Output = C;
for i = 1 : size(part,2)
   empty_index = find(C(part(1,i)+1,:) == 2);
   Output(part(1,i)+1,empty_index(1)) = zerone;
end
end



% function signal = modulator(Input,M,Ts,fc,fs)
% signal = [];
% len = floor(log2(M));
% for i = 1 : length(Input)
%    m = Input(i);
%    t1 = 0:(1/fs):m*Ts/M;
%    value1 = sqrt(2*M/m*Ts)*cos(2*pi*fc*t1);
%    zero_numbers = Ts*fs - length(value1);
%    zero_part = zeros(1,zero_numbers);
%    pulse = [value1 zero_part];
%    
%    signal = [signal pulse];
%    
% end
% 
% end

function signal = modulator(Input,M,Ts,fc,fs)
signal = zeros(1,floor(length(Input)*Ts*fs));
for i = 1 : length(Input)
   m = Input(i);
   t1 = 0:(1/fs):m*Ts/M;
   value1 = sqrt(2*M/m*Ts)*cos(2*pi*fc*t1);
   pulse = zeros(1,Ts*fs);
   pulse(1:length(value1)) = value1;
   signal(1,((i-1)*Ts*fs+1):( (i-1)*Ts*fs+Ts*fs) ) = pulse(1,1:Ts*fs);
   
end

end

% function corr_vector = demodulator(signal,Ts,fs,basis1,basis2)
% N = Ts*fs;
% corr_vector = [];
% while length(signal) >= N
%    corr_basis1 = corr2(signal(1:N),basis1);
%    corr_basis2 = corr2(signal(1:N),basis2);
%    corr_vector = [corr_vector [corr_basis1;corr_basis2]];
%    signal(1:N)=[];
% end
% end

function corr_vector = demodulator(signal,Ts,fs,basis1,basis2)
N = Ts*fs;
corr_vector = zeros( 2 , ceil(length(signal)/N) );
k=1;
   for i=1:N:length(signal)
       
       corr_basis1 = corr2(signal(i:N+i-1),basis1);
       corr_basis2 = corr2(signal(i:N+i-1),basis2);  
       corr_vector(1,k) =corr_basis1;
       corr_vector(2,k) = corr_basis2;
       k = k + 1;
   end

end

function y = detector(corr_vector,detection_table)
y = zeros(1,size(corr_vector,2));
threshold = (detection_table(1,1) + detection_table(1,2))/2;
for k = 1 : size(corr_vector,2)
    if corr_vector(1,k) > threshold && corr_vector(2,k) < threshold
        y(k) = 1;
    elseif corr_vector(1,k) < threshold && corr_vector(2,k) > threshold
        y(k) = 2;
    elseif abs(corr_vector(1,k)) > abs(corr_vector(2,k))
        y(k) = 1;
    else
        y(k) = 2;
    end
end

end


% function output_pic = source_decoder(encoded,table)
% 
% symbols = [];
% string = encoded;
% k=1;
% while 1
%     my_index = 0;
%    for i = 1:256
%       
%        index = find(table(i,:) == 2);
%        if index(1)-1 <= length(string)
%            if sum(string(1:index(1)-1) == table(i,1:index(1)-1)) == length(string(1:index(1)-1))
%               symbols = [symbols i-1];
%               string(1:index(1)-1) = [];
%               k=k+1
%               my_index = 1;
%               break
%            end
%        end
%    end
%    
%    if length(string) < 1
%        break
%    elseif my_index==0 && index(1)-1 <= length(string)
%        symbols = [symbols 0];
%               string(1:index(1)-1) = [];
%               k = k+1
%    elseif my_index==0 
%        string = [];
%               k=k+1
%    end
% end
% output_pic = zeros(64,64);
% for i = 1 : length(symbols)
%    row = ceil(i/64);
%    column = i - (row-1)*64;
%    output_pic(row,column) = symbols(i);
% end
% 
% end


function output_pic = source_decoder(encoded,table)

symbols = [];
string = encoded;
while 1
   diff = length(encoded)*ones(1,256);
   for i = 1:256
       index = find(table(i,:) == 2);
       if index(1)-1 <= length(string)
           diff(1,i) = length(string(1:index(1)-1))-sum(string(1:index(1)-1) == table(i,1:index(1)-1));
           if sum(string(1:index(1)-1) == table(i,1:index(1)-1)) == length(string(1:index(1)-1))
              symbols = [symbols i-1];
              string(1:index(1)-1) = [];
              break
           end
       end
       if i == 256
           length(symols)
           minimum_diff = find(diff == min(diff));
           row = minimum_diff(1);
           symbols = [symbols row-1];
           index = find(table(row,:) == 2);
           if length(string) >= index(1)-1
             string(1:index(1)-1) = [];
           end
       end
   end
   if length(string) < 10
       break
   end
end
output_pic = zeros(64,64);
for i = 1 : length(symbols)
   row = ceil(i/64);
   column = i - (row-1)*64;
   output_pic(row,column) = symbols(i);
end
end

function [pic_reconstructed,corr_vector] = reconstruction(modulated_signal,BPF_Channel,Ts,fs,basis1,basis2,detection_table,table)
% Channel
received_signal = filter(BPF_Channel,1,modulated_signal);

% Demodulator
corr_vector = demodulator(received_signal,Ts,fs,basis1,basis2);

% Detector
symbols_reconstructed = detector(corr_vector,detection_table);
bit_string = zeros(1,length(symbols_reconstructed));
for i = 1 : length(symbols_reconstructed)
    if symbols_reconstructed(i) == 1
        bit_string(i) = 0;
    else
        bit_string(i) = 1;
    end
end

% Source decoder
pic_reconstructed = source_decoder(bit_string,table);

end



function y = f_trans(x,N)
y_init = fft(x,N);y = fftshift(y_init);
end
